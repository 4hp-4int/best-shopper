package splash

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

// Client Object
type Client struct {
}

// ExecuteLuaRequest a Splash Run Execute Request Body
type ExecuteLuaRequest struct {
	LuaSource string `json:"lua_source"`
	URL       string `json:"url"`
	Timeout   int    `json:"timeout"`
}

// ExecuteLua runs Arbitrary Lua Crawling Code in Splash
func (s Client) ExecuteLua(filename string, url string) bool {
	var filePath = fmt.Sprintf("%s/%s", os.GetEnv("LUA_FOLDER"), filename)
	contents, err := ioutil.ReadFile(filePath)

	if err != nil {
		log.Fatal(err)
	}
	luaCode := string(contents)
	requestBody := &ExecuteLuaRequest{
		LuaSource: luaCode,
		URL:       url,
		Timeout:   3600,
	}

	payloadBuffer := new(bytes.Buffer)
	json.NewEncoder(payloadBuffer).Encode(requestBody)

	resp, err := http.Post("http://localhost:8050/run", "application/json", payloadBuffer)
	if err != nil {
		log.Fatalln(err)
	}

	fmt.Println(resp.StatusCode)
	return true
}
