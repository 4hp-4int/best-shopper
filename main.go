package main

import (
	"flag"
	"fmt"
	"strings"
	"wps5/splash"
)

type products []string

func (p *products) String() string {
	return fmt.Sprint(*p)
}

// Set required for parsing -u argument properly.
func (p *products) Set(value string) error {
	for _, url := range strings.Split(value, " ") {
		*p = append(*p, url)
	}

	return nil
}

var urlFlag products

func init() {
	flag.Var(&urlFlag, "u", "Provide a product page url(s).")
}

func main() {
	flag.Parse()

	fmt.Println(urlFlag)
	var client = splash.Client{}
	client.ExecuteLua("best-buy.lua", urlFlag[0])
}
