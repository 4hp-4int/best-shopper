This script interacts with SplashJS Server(s). It uses lua scripts to run shopping checkout tasks automatically.

The purpose of the SplashJS Server is to render javascript pages dynamically, with the help of lua scripts we can dynamically check pages after 
page load, refresh until we find certain page elements etc... The usage of Lua Scripts and SplashJs in this way allow this code to remain flexible
as websites changes rapidly. 

I'm intending to make use of a captcha service, which unfortunately just uses a real human to solve the captcha and return the answer as a key.
This isn't very fast but maybe we won't hit a captcha in every case.



Currently in order to load a page you must very acutely determine which requests are safe to load, vs not for any given website. This currently relies on adblock-plus filter rules.
