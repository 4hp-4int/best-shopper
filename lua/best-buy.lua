-- Best Buy Add to cart/Checkout script for a given product page.
    json = require("json")
    splash.response_body_enabled = true
    splash.images_enabled = false


    -- Go to Product Page URL, 
    assert(splash:go(args.url))
    assert(splash:wait(0.5))
    cookies = splash:get_cookies()
    splash:init_cookies(cookies)

    -- Callback to check when we get a successful add.
    local addToCartSuccess = false
    local button = assert(splash:select('.add-to-cart-button'))
    local numClicks = 0
    splash:on_response(function(response)
        if string.find(response.url, "addToCart") ~= nil then
            if response.ok == true then
                addToCartSuccess = true
            end
        end
    end)

    -- Click until success
    while (not(addToCartSuccess))
    do        
        numClicks = numClicks + 1
        button:mouse_click()
        splash:wait{time=1.0}
    end

    return {
        html = splash:html(),
        png = splash:png(),
        har = splash:har(),
        clicks = numClicks,
    }
